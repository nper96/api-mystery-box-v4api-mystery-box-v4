Mystery Box API v4
===

¯\_(ツ)_/¯

NPM Command
---

Command | Description 
--------|------------
`npm start` | Runs application with hot-reload for local development
`npm run lint` | Runs linter to check for error
`npm run lint-fix` | Runs linter to fix the errors _(Warning: This will change your source code!)_
`npm test` | Runs the unit tests
`npm run coverage` | Checks code coverage of unit tests
`npm run coverage-show` | Runs the code coverage of unit tests and print the results
`npm run create-nyc-report-dir` | Creates the coverage report directory that is not created on some machines and leads to coverage reporting failure
`npm run check` | Runs all checks performed during the build
`npm run build` | Transpiles the source code to Node-compliant code
`npm run serve` | Runs the transpiled code

OpenShift Build & Deploy
---

```
# Build
oc apply -f openshift/build.yaml
oc start-build buildConfig/api-mystery-box-v4 --from-dir=. --wait --follow

# Deploy
oc apply -f openshift/deploy.yaml
oc rollout latest deploymentConfig/api-mystery-box-v4
oc rollout status deploymentConfig/api-mystery-box-v4 --watch
```