// eslint-disable-next-line no-unused-vars
import { expect } from "chai";
import MysteryService from "../../src/service/MysteryService";

describe("MysteryService", () => {
  // eslint-disable-next-line no-unused-vars
  const service = new MysteryService();

  // Uncomment the test cases related to your feature

  describe("gadgets", () => {
    it("returns the correct value #1", async () => {
      // expect(await service.gadgets({ text: "sohrab" })).to.deep.equal({
      //   text: "barhos"
      // });
    });

    it("returns the correct value #2", async () => {
      // expect(await service.gadgets({ text: "123456" })).to.deep.equal({
      //   text: "654321"
      // });
    });
  });

  describe("widgets", () => {
    it("returns the correct value #1", async () => {
      // expect(await service.widgets({ text: "1 + 4" })).to.deep.equal({
      //   text: "5"
      // });
    });

    it("returns the correct value #2", async () => {
      // expect(await service.widgets({ text: "3 + 234" })).to.deep.equal({
      //   text: "237"
      // });
    });
  });

  describe("gizmos", () => {
    it("returns the correct value #1", async () => {
      // expect(await service.gizmos({ text: "11" })).to.deep.equal({
      //   text: "121"
      // });
    });

    it("returns the correct value #2", async () => {
      // expect(await service.gizmos({ text: "25" })).to.deep.equal({
      //   text: "625"
      // });
    });
  });

  describe("whatchamacallits", () => {
    it("returns the correct value #1", async () => {
      // expect(await service.whatchamacallits({ text: "hey" })).to.deep.equal({
      //   text: "HEY!!!"
      // });
    });

    it("returns the correct value #2", async () => {
      // expect(await service.whatchamacallits({ text: "what" })).to.deep.equal({
      //   text: "WHAT!!!!"
      // });
    });
  });

  describe("doodads", () => {
    it("returns the correct value #1", async () => {
      // expect(await service.doodads({ text: "bye" })).to.deep.equal({
      //   text: "byebyebye"
      // });
    });

    it("returns the correct value #2", async () => {
      // expect(await service.doodads({ text: "good" })).to.deep.equal({
      //   text: "goodgoodgoodgood"
      // });
    });
  });

  describe("doohickeys", () => {
    it("returns the correct value #1", async () => {
      // expect(await service.doohickeys({ text: "swat" })).to.deep.equal({
      //   text: "S.W.A.T."
      // });
    });

    it("returns the correct value #2", async () => {
      // expect(await service.doohickeys({ text: "dpe" })).to.deep.equal({
      //   text: "D.P.E."
      // });
    });
  });

  describe("thingamabobs", () => {
    it("returns the correct value #1", async () => {
      // expect(await service.thingamabobs({ text: "1,2,3,4,5,6" })).to.deep.equal({
      //   text: "21"
      // });
    });

    it("returns the correct value #2", async () => {
      // expect(await service.thingamabobs({ text: "4,66,7" })).to.deep.equal({
      //   text: "77"
      // });
    });
  });

  describe("gubbins", () => {
    it("returns the correct value #1", async () => {
      // expect(await service.gubbins({ text: "23>24" })).to.deep.equal({
      //   text: "false"
      // });
    });

    it("returns the correct value #2", async () => {
      // expect(await service.gubbins({ text: "23==23" })).to.deep.equal({
      //   text: "true"
      // });
    });
  });

  describe("stuff", () => {
    it("returns the correct value #1", async () => {
      // expect(await service.gubbins({ text: "Hello World" })).to.deep.equal({
      //   text: "Hello   World"
      // });
    });

    it("returns the correct value #2", async () => {
      // expect(await service.gubbins({ text: "this is awesome" })).to.deep.equal({
      //   text: "this   is   awesome"
      // });
    });
  });

  describe("whatsis", () => {
    it("returns the correct value #1", async () => {
      // expect(await service.gubbins({ text: "123" })).to.deep.equal({
      //   text: "1111011"
      // });
    });

    it("returns the correct value #2", async () => {
      // expect(await service.gubbins({ text: "54" })).to.deep.equal({
      //   text: "110110"
      // });
    });
  });

  describe("thingumajigs", () => {
    it("returns the correct value #1", async () => {
      // expect(await service.gubbins({ text: "caesar" })).to.deep.equal({
      //   text: "dbftbs"
      // });
    });

    it("returns the correct value #2", async () => {
      // expect(await service.gubbins({ text: "sohrab" })).to.deep.equal({
      //   text: "tpisbc"
      // });
    });
  });

  describe("gimmicks", () => {
    it("returns the correct value #1", async () => {
      // expect(await service.gubbins({ text: "sohrab" })).to.deep.equal({
      //   text: "s|o|h|r|a|b"
      // });
    });

    it("returns the correct value #2", async () => {
      // expect(await service.gubbins({ text: "this is awesome" })).to.deep.equal({
      //   text: "t|h|i|s|i|s|a|w|e|s|o|m|e"
      // });
    });
  });

  describe("doojiggers", () => {
    it("returns the correct value #1", async () => {
      // expect(await service.gubbins({ text: "3,5,1,2,4" })).to.deep.equal({
      //   text: "1,2,3,4,5"
      // });
    });

    it("returns the correct value #2", async () => {
      // expect(await service.gubbins({ text: "23,5,67,8" })).to.deep.equal({
      //   text: "5,8,23,67"
      // });
    });
  });

  describe("whatnots", () => {
    it("returns the correct value #1", async () => {
      // expect(await service.gubbins({ text: "red fox jumps" })).to.deep.equal({
      //   text: "fox jumps red"
      // });
    });

    it("returns the correct value #2", async () => {
      // expect(await service.gubbins({ text: "this is very awesome" })).to.deep.equal({
      //   text: "awesome is this very"
      // });
    });
  });

  describe("contraptions", () => {
    it("returns the correct value #1", async () => {
      // expect(await service.gubbins({ text: "11am" })).to.deep.equal({
      //   text: "11"
      // });
    });

    it("returns the correct value #2", async () => {
      // expect(await service.gubbins({ text: "5pm" })).to.deep.equal({
      //   text: "17"
      // });
    });
  });

  describe("bitsandbobs", () => {
    it("returns the correct value #1", async () => {
      // expect(await service.gubbins({ text: "19930" })).to.deep.equal({
      //   text: "5h 32m 10s"
      // });
    });

    it("returns the correct value #2", async () => {
      // expect(await service.gubbins({ text: "610" })).to.deep.equal({
      //   text: "10m 10s"
      // });
    });
  });
});
