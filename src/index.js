import httpServer from "./http/httpServer";
import MysteryService from "./service/MysteryService";

// app config
const LOG_LEVEL = process.env.LOG_LEVEL || "info";
const HTTP_PORT = process.env.HTTP_PORT || process.env.PORT || "8085";
const HTTP_BASE_PATH = process.env.HTTP_BASE_PATH || "";

// start http server
httpServer(new MysteryService(), {
  httpPort: HTTP_PORT,
  httpBasePath: HTTP_BASE_PATH,
  logLevel: LOG_LEVEL
});
