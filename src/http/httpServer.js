import fs from "fs";
import path from "path";
import fastify from "fastify";
import fastifySensible from "fastify-sensible";

import fastifyOpenapi from "../fastify-openapi";
import { notFoundHandler, defaultErrorHandler } from "./error";

const BASE_DIR = path.join(__dirname, "..", "..");
const API_SPEC_YAML_PATH = path.join(BASE_DIR, "api.yaml");

const apiSpecYaml = fs.readFileSync(API_SPEC_YAML_PATH);
const apiSpecHtml = fs.readFileSync(path.join(BASE_DIR, "public", "docs.html"));

/**
 * Configures and starts a Fastify server
 *
 * @param {string} httpPort The HTTP port where the server is listening
 * @param {string} httpBasePath The HTTP base path applied to all resources
 * @param {string} logLevel The log level
 */
export default function httpServer(
  service,
  { httpPort, httpBasePath = "", logLevel }
) {
  const server = fastify({
    logger: {
      level: logLevel,
      redact: ["req.headers.authorization"]
    }
  });

  server.register(fastifySensible, { errorHandler: false });

  // serve API
  server.register(fastifyOpenapi, {
    apiSpec: API_SPEC_YAML_PATH,
    services: {
      "*": service
    },
    basePath: `${httpBasePath}/api`
  });

  // serve API spec
  server.get(`${httpBasePath}/docs`, (request, reply) => {
    reply.type("text/html").send(apiSpecHtml);
  });
  server.get(`${httpBasePath}/apidocs.yaml`, (request, reply) => {
    reply.type("text/yaml").send(apiSpecYaml);
  });

  // error handling
  server.setNotFoundHandler(notFoundHandler);
  server.setErrorHandler(defaultErrorHandler);

  // health check
  server.get("/health", { logLevel: "warn" }, async () => "");

  // start server
  (async () => {
    try {
      await server.listen(httpPort, "0.0.0.0");
      server.log.info(
        `🚀 API listening on http://0.0.0.0:${httpPort}${httpBasePath}/api`
      );
      server.log.info(`📚 API docs @ http://0.0.0.0:${httpPort}/docs`);
    } catch (err) {
      server.log.error(err.stack);
      process.exit(1);
    }
  })();

  return server;
}
